<?php
/*Route::get('/', function () { return redirect('/admin/home'); });*/

Route::group(['middleware' => ['auth', 'loadjscss']], function () {
Route::get('/', 'HomeController@index');
Route::get('admin/home', 'HomeController@index');
});


Route::group(['middleware' => ['loadjscss'], 'namespace' => 'Auth'], function () {
// Authentication Routes...
Route::get('login',  ['uses' =>'LoginController@showLoginForm', 'as' => 'login']);
Route::post('login',  ['uses' =>'LoginController@login',  'as' => 'auth.login']);
Route::post('logout',  ['uses' =>'LoginController@logout',   'as' => 'auth.logout']);

// Change Password Routes...
Route::get('change_password',  ['uses' =>'ChangePasswordController@showChangePasswordForm', 'as' => 'auth.change_password']);
Route::patch('change_password',  ['uses' =>'ChangePasswordController@changePassword', 'as' => 'auth.change_password']);

// Password Reset Routes...
Route::get('password/reset',  ['uses' =>'ForgotPasswordController@showLinkRequestForm', 'as' => 'auth.password.reset']);
Route::post('password/email',  ['uses' =>'ForgotPasswordController@sendResetLinkEmail', 'as' => 'auth.password.reset']);
Route::get('password/reset/{token}',  ['uses' =>'ResetPasswordController@showResetForm', 'as' => 'password.reset']);
Route::post('password/reset', ['uses' => 'ResetPasswordController@reset', 'as' => 'auth.password.reset']);

});



Route::group(['middleware' => ['auth','loadjscss'], 'prefix' => 'admin','namespace' => 'Admin', 'as' => 'admin.'], function () {

    // Role
    Route::resource('roles', 'RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'RolesController@massDestroy', 'as' => 'roles.mass_destroy']);

    // User
    Route::resource('users', 'UsersController');
    Route::post('users_mass_destroy', ['uses' => 'UsersController@massDestroy', 'as' => 'users.mass_destroy']);

    // Project
    Route::resource('projects', 'ProjectsController');
    Route::post('projects_mass_destroy', ['uses' => 'ProjectsController@massDestroy', 'as' => 'projects.mass_destroy']);
    Route::post('projects_restore/{id}', ['uses' => 'ProjectsController@restore', 'as' => 'projects.restore']);
    Route::delete('projects_perma_del/{id}', ['uses' => 'ProjectsController@perma_del', 'as' => 'projects.perma_del']);

    // Task
    Route::resource('tasks', 'TasksController');
    Route::post('tasks_mass_destroy', ['uses' => 'TasksController@massDestroy', 'as' => 'tasks.mass_destroy']);
    Route::post('tasks_restore/{id}', ['uses' => 'TasksController@restore', 'as' => 'tasks.restore']);
    Route::delete('tasks_perma_del/{id}', ['uses' => 'TasksController@perma_del', 'as' => 'tasks.perma_del']);

    // Assign Task
    Route::resource('assign_tasks', 'AssignTasksController');
    Route::post('assign_tasks_mass_destroy', ['uses' => 'AssignTasksController@massDestroy', 'as' => 'assign_tasks.mass_destroy']);
    Route::post('assign_tasks_restore/{id}', ['uses' => 'AssignTasksController@restore', 'as' => 'assign_tasks.restore']);
    Route::delete('assign_tasks_perma_del/{id}', ['uses' => 'AssignTasksController@perma_del', 'as' => 'assign_tasks.perma_del']);

    
});

/* Event::listen('illuminate.query', function($query)
 {
     echo "<pre>";
     var_dump($query);
     echo "</pre>";
 });  
*/