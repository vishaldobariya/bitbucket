<script src="{{ url('js/jquery-1.11.3.min.js') }}"></script>

      <!-- Datatables -->
@if($datatableLoad)
<script>
    window.deleteButtonTrans = '{{ trans("translate.delete_selected") }}';
    window.copyButtonTrans = '{{ trans("translate.copy") }}';
    window.csvButtonTrans = '{{ trans("translate.csv") }}';
    window.excelButtonTrans = '{{ trans("translate.excel") }}';
    window.pdfButtonTrans = '{{ trans("translate.pdf") }}';
    window.printButtonTrans = '{{ trans("translate.print") }}';
    window.colvisButtonTrans = '{{ trans("translate.colvis") }}';
</script>
<script src="{{ url('datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('datatable/buttons.flash.min.js') }}"></script>
<script src="{{ url('datatable/jszip.min.js') }}"></script>
<script src="{{ url('datatable/pdfmake.min.js') }}"></script>
<script src="{{ url('datatable/vfs_fonts.js') }}"></script>
<script src="{{ url('datatable/buttons.html5.min.js') }}"></script>
<script src="{{ url('datatable/buttons.print.min.js') }}"></script>
<script src="{{ url('datatable/buttons.colVis.min.js') }}"></script>
<script src="{{ url('datatable/dataTables.select.min.js') }}"></script>
@endif
    <!-- End Datatables -->

<script src="{{ url('js/jquery-ui.min.js') }}"></script>
<script src="{{ url('adminlte/js/bootstrap.min.js') }}"></script>
<script src="{{ url('adminlte/js/select2.full.min.js') }}"></script>
<script src="{{ url('adminlte/js/main.js') }}"></script>

<script src="{{ url('adminlte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ url('adminlte/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ url('adminlte/js/app.min.js') }}"></script>
<script>
    window._token = '{{ csrf_token() }}';
</script>
<script>
    $.extend(true, $.fn.dataTable.defaults, {
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/English.json"
        }
    });

     

</script>

<script>
    $(function(){
        /** add active class and stay opened when selected */
        var url = window.location;

        // for sidebar menu entirely but not cover treeview
        $('ul.sidebar-menu a').filter(function() {
            return this.href == url;
        }).parent().addClass('active');

        $('ul.treeview-menu a').filter(function() {
            return this.href == url;
        }).parent().addClass('active');

        // for treeview
        $('ul.treeview-menu a').filter(function() {
             return this.href == url;
        }).parentsUntil('.sidebar-menu > .treeview-menu').addClass('menu-open').css('display', 'block');
    });
</script>

 



@yield('javascript')
