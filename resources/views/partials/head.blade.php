<meta charset="utf-8">
<title>
    @lang('translate.maintitle')
</title>

<meta http-equiv="X-UA-Compatible"
      content="IE=edge">
<meta content="width=device-width, initial-scale=1.0"
      name="viewport"/>
<meta http-equiv="Content-type"
      content="text/html; charset=utf-8">

<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ url('common/css/font-awesome.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ url('common/css/ionicons.min.css') }}">

<link href="{{ url('adminlte/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ url('common/css/select2.min.css') }}"/>
<link href="{{ url('adminlte/css/AdminLTE.min.css') }}" rel="stylesheet">
<link href="{{ url('adminlte/css/custom.css') }}" rel="stylesheet">
<link href="{{ url('adminlte/css/skins/skin-blue.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ url('common/css/jquery-ui.css') }}">
	<!-- Datatable --> 
@if($datatableLoad)
<link rel="stylesheet" href="{{ url('datatable/jquery.dataTables.min.css') }}"/>
<link rel="stylesheet" href="{{ url('datatable/select.dataTables.min.css') }}"/>
<link rel="stylesheet" href="{{ url('datatable/buttons.dataTables.min.css') }}"/>
@endif
	<!-- End Datatable -->
<link href="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>
