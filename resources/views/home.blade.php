@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('translate.dashboard')</div>

                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-user-circle-o"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Administator</span>
                              <span class="info-box-number">{{ $totaladmin }}</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box">
                            <span class="info-box-icon bg-red"><i class="fa fa-user-o"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Team Leader</span>
                              <span class="info-box-number">{{ $totaltl }}</span>
                            </div>
                          </div>
                        </div>
                        <!-- /.col -->
                    </div> <!-- End row -->

                    <div class="row">

                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa fa-product-hunt"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Pojects</span>
                              <span class="info-box-number">{{ $projectcount }}</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-bar-chart"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">Tasks</span>
                              <span class="info-box-number">{{ $taskcount }}</span>
                            </div>
                          </div>
                        </div>
                        <!-- /.col -->
                    </div> <!-- End row -->
                     @if (count($lasttentask) > 0)
                    <div class="row">
                        <div class="box box-info">
                            <div class="box-header with-border">
                              <h3 class="box-title">Last 10 Done Task</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                              <div class="table-responsive">
                                <table class="table no-margin">
                                  <thead>
                                  <tr>
                                    <th>Team Leader</th>
                                    <th>Project</th>
                                    <th>Task Name</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                 
                                    @foreach ($lasttentask as  $value)
                                      @if(!empty($value['task']))
                                      <tr>
                                        <td>{{ $value['username'] }}</td>
                                        <td>{{ $value['projectname'] }}</td>
                                        <td>{{ $value['taskname'] }}</td>
                                      </tr>
                                     @endif
                                    @endforeach
                                  </tbody>
                                </table>
                              </div>
                              <!-- /.table-responsive -->
                            </div>
                          </div>
                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
@endsection
