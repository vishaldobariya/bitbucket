@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('translate.task.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('translate.view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('translate.taskss.fields.project')</th>
                            <td field-key='project'>{{ $task->project->projectname ?? '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('translate.taskss.fields.taskname')</th>
                            <td field-key='taskname'>{{ $task->taskname }}</td>
                        </tr>
                        <tr>
                            <th>@lang('translate.taskss.fields.description')</th>
                            <td field-key='description'>{!! $task->description !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('translate.taskss.fields.status')</th>
                            <td field-key='status'>{{ $task->status }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#assign_task" aria-controls="assign_task" role="tab" data-toggle="tab">Assign task</a></li>


<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="assign_task">
<table class="table table-bordered table-striped {{ count($assign_tasks) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('translate.assign-tasks.fields.project')</th>
                        <th>@lang('translate.assign-tasks.fields.task')</th>
                       
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($assign_tasks) > 0)
            @foreach ($assign_tasks as $assign_task)
                <tr data-entry-id="{{ $assign_task->id }}">
                    <td field-key='project'>{{ $assign_task->project->projectname ?? '' }}</td>
                                <td field-key='task'>{{ $assign_task->task->taskname ?? '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('assign_task_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.assign_tasks.restore', $assign_task->id])) !!}
                                    {!! Form::submit(trans('translate.restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('assign_task_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.assign_tasks.perma_del', $assign_task->id])) !!}
                                    {!! Form::submit(trans('translate.permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('assign_task_view')
                                    <a href="{{ route('admin.assign_tasks.show',[$assign_task->id]) }}" class="btn btn-xs btn-primary">@lang('translate.view')</a>
                                    @endcan
                                    @can('assign_task_edit')
                                    <a href="{{ route('admin.assign_tasks.edit',[$assign_task->id]) }}" class="btn btn-xs btn-info">@lang('translate.edit')</a>
                                    @endcan
                                    @can('assign_task_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.assign_tasks.destroy', $assign_task->id])) !!}
                                    {!! Form::submit(trans('translate.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8">@lang('translate.no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.tasks.index') }}" class="btn btn-default">@lang('translate.back_to_list')</a>
        </div>
    </div>
@stop

@section('javascript')
    @parent
    <script src="//cdn.ckeditor.com/4.5.4/full/ckeditor.js"></script>
    <script>
        $('.editor').each(function () {
                  CKEDITOR.replace($(this).attr('id'),{
                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
            });
        });
    </script>

@stop
