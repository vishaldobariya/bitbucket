@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('translate.assign-tasks.title')</h3>
    
    {!! Form::model($assign_task, ['method' => 'PUT', 'route' => ['admin.assign_tasks.update', $assign_task->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('translate.edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('project_id', trans('translate.assign-tasks.fields.project').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('project_id', $projects, old('project_id'), ['class' => 'form-control select2', 'required' => '',  'onchange'=>"handleproj()" ]) !!}
                    @if($errors->has('project_id'))
                        <p class="help-block">
                            {{ $errors->first('project_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('task_id', trans('translate.assign-tasks.fields.task').'*', ['class' => 'control-label']) !!}
                   <select class="form-control" id="task_id" name="task_id">
                        <option value="">{{ trans('translate.please_select') }}</option>
                        @foreach($tasks as $key => $value)
                        <option value="{{ $value->id }}" class="handleproj project{{$value->project_id}}" selected="{{ ($value->id  == $assign_task['task_id'])?'selected':'' }}">{{ $value->taskname }}</option>
                        @endforeach
                    </select>

                    @if($errors->has('task_id'))
                        <p class="help-block">
                            {{ $errors->first('task_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('user_id', trans('translate.assign-tasks.fields.user').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('user_id', $users, old('user_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                    @if($errors->has('user_id'))
                        <p class="help-block">
                            {{ $errors->first('user_id') }}
                        </p>
                    @endif
                </div>
            </div>
            
            
        </div>
    </div>

    {!! Form::submit(trans('translate.update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
<script type="text/javascript">
    function handleproj() {
        var selectproject = $("#project_id").val();
        $('#task_id').val('');
       // $("#task_id").select2('destroy');
        $("#task_id .handleproj").hide();
        $(".project"+selectproject).show();
        //$("#task_id").select2();
    }
</script>
@stop

