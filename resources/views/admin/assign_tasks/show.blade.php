@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('translate.assign-tasks.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('translate.view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('translate.assign-tasks.fields.project')</th>
                            <td field-key='project'>{{ $assign_task->project->projectname ?? '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('translate.assign-tasks.fields.task')</th>
                            <td field-key='task'>{{ $assign_task->task->taskname ?? '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('translate.assign-tasks.fields.user')</th>
                            <td field-key='user'>{{ $assign_task->user->name ?? '' }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.assign_tasks.index') }}" class="btn btn-default">@lang('translate.back_to_list')</a>
        </div>
    </div>
@stop


