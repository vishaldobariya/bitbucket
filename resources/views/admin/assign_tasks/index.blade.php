@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('translate.assign-tasks.title')</h3>
    @can('assign_task_create')
    <p>
        <a href="{{ route('admin.assign_tasks.create') }}" class="btn btn-success">@lang('translate.add_new')</a>
        
    </p>
    @endcan

    @can('assign_task_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.assign_tasks.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('translate.all')</a></li> |
            <li><a href="{{ route('admin.assign_tasks.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('translate.trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('translate.list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($assign_tasks) > 0 ? 'datatable' : '' }} @can('assign_task_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('assign_task_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('translate.assign-tasks.fields.project')</th>
                        <th>@lang('translate.assign-tasks.fields.task')</th>
                         <th>@lang('translate.assign-tasks.fields.user')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($assign_tasks) > 0)
                        @foreach ($assign_tasks as $assign_task)
                            <tr data-entry-id="{{ $assign_task->id }}">
                                @can('assign_task_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan

                                <td field-key='project'>{{ $assign_task->project->projectname ?? '' }}</td>
                                <td field-key='task'>{{ $assign_task->task->taskname ?? '' }}</td>
                                <td field-key='user'>{{ $assign_task->user->name ?? '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('assign_task_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.assign_tasks.restore', $assign_task->id])) !!}
                                    {!! Form::submit(trans('translate.restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('assign_task_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.assign_tasks.perma_del', $assign_task->id])) !!}
                                    {!! Form::submit(trans('translate.permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('assign_task_view')
                                    <a href="{{ route('admin.assign_tasks.show',[$assign_task->id]) }}" class="btn btn-xs btn-primary">@lang('translate.view')</a>
                                    @endcan
                                    @can('assign_task_edit')
                                    <a href="{{ route('admin.assign_tasks.edit',[$assign_task->id]) }}" class="btn btn-xs btn-info">@lang('translate.edit')</a>
                                    @endcan
                                    @can('assign_task_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.assign_tasks.destroy', $assign_task->id])) !!}
                                    {!! Form::submit(trans('translate.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">@lang('translate.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('assign_task_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.assign_tasks.mass_destroy') }}'; @endif
        @endcan

    </script>
@endsection