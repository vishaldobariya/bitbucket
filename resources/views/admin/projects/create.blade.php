@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('translate.project')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.projects.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('translate.create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('projectname', trans('translate.project').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('projectname', old('projectname'), ['class' => 'form-control', 'placeholder' => 'Project Name']) !!}
                    @if($errors->has('projectname'))
                        <p class="text-danger">
                            {{ $errors->first('projectname') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('translate.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

