@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('translate.projectss.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('translate.view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('translate.project')</th>
                            <td field-key='projectname'>{{ $project->projectname }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#task" aria-controls="task" role="tab" data-toggle="tab">Task</a></li>
<li role="presentation" class=""><a href="#assign_task" aria-controls="assign_task" role="tab" data-toggle="tab">Assign task</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="task">
<table class="table table-bordered table-striped {{ count($tasks) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('translate.taskss.fields.project')</th>
                        <th>@lang('translate.taskss.title')</th>
                        <th>@lang('translate.taskss.fields.description')</th>
                        <th>@lang('translate.taskss.fields.status')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($tasks) > 0)
            @foreach ($tasks as $task)
                <tr data-entry-id="{{ $task->id }}">
                    <td field-key='project'>{{ $task->project->projectname ?? '' }}</td>
                                <td field-key='taskname'>{{ $task->taskname }}</td>
                                <td field-key='description'>{!! $task->description !!}</td>
                                <td field-key='status'>{{ $task->status }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('task_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.tasks.restore', $task->id])) !!}
                                    {!! Form::submit(trans('translate.restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('task_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.tasks.perma_del', $task->id])) !!}
                                    {!! Form::submit(trans('translate.permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('task_view')
                                    <a href="{{ route('admin.tasks.show',[$task->id]) }}" class="btn btn-xs btn-primary">@lang('translate.view')</a>
                                    @endcan
                                    @can('task_edit')
                                    <a href="{{ route('admin.tasks.edit',[$task->id]) }}" class="btn btn-xs btn-info">@lang('translate.edit')</a>
                                    @endcan
                                    @can('task_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.tasks.destroy', $task->id])) !!}
                                    {!! Form::submit(trans('translate.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">@lang('translate.no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
<div role="tabpanel" class="tab-pane " id="assign_task">
<table class="table table-bordered table-striped {{ count($assign_tasks) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('translate.assign-tasks.fields.project')</th>
                        <th>@lang('translate.assign-tasks.fields.task')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
        </tr>
    </thead>

    <tbody>
        @if (count($assign_tasks) > 0)
            @foreach ($assign_tasks as $assign_task)
                <tr data-entry-id="{{ $assign_task->id }}">
                    <td field-key='project'>{{ $assign_task->project->projectname ?? '' }}</td>
                                <td field-key='task'>{{ $assign_task->task->taskname ?? '' }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('assign_task_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.assign_tasks.restore', $assign_task->id])) !!}
                                    {!! Form::submit(trans('translate.restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('assign_task_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.assign_tasks.perma_del', $assign_task->id])) !!}
                                    {!! Form::submit(trans('translate.permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('assign_task_view')
                                    <a href="{{ route('admin.assign_tasks.show',[$assign_task->id]) }}" class="btn btn-xs btn-primary">@lang('translate.view')</a>
                                    @endcan
                                    @can('assign_task_edit')
                                    <a href="{{ route('admin.assign_tasks.edit',[$assign_task->id]) }}" class="btn btn-xs btn-info">@lang('translate.edit')</a>
                                    @endcan
                                    @can('assign_task_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",
                                        'route' => ['admin.assign_tasks.destroy', $assign_task->id])) !!}
                                    {!! Form::submit(trans('translate.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8">@lang('translate.no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.projects.index') }}" class="btn btn-default">@lang('translate.back_to_list')</a>
        </div>
    </div>
@stop


