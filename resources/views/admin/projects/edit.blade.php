@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('translate.project')</h3>
    
    {!! Form::model($project, ['method' => 'PUT', 'route' => ['admin.projects.update', $project->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('translate.edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('projectname', trans('translate.project').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('projectname', old('projectname'), ['class' => 'form-control', 'placeholder' => 'Project Name']) !!}
                    @if($errors->has('projectname'))
                        <p class="help-block">
                            {{ $errors->first('projectname') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('translate.update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

