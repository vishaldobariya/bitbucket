<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationshipsToAssignTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assign_tasks', function(Blueprint $table) {
            if (!Schema::hasColumn('assign_tasks', 'project_id')) {
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id', 'projectwithassigntask')->references('id')->on('projects')->onDelete('cascade');
                }
                if (!Schema::hasColumn('assign_tasks', 'task_id')) {
                $table->integer('task_id')->unsigned()->nullable();
                $table->foreign('task_id', 'taskwithassigntask')->references('id')->on('tasks')->onDelete('cascade');
                }
                if (!Schema::hasColumn('assign_tasks', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', 'assigntaskwithdeveloper')->references('id')->on('users')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assign_tasks', function(Blueprint $table) {
            
        });
    }
}
