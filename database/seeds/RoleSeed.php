<?php

use Illuminate\Database\Seeder;

use App\Model\Role;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'title' => 'Administrator'],
            ['id' => 2, 'title' => 'Team Leader']

        ];

        foreach ($items as $item) {
            Role::create($item);
        }
    }
}
