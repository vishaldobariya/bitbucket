<?php

use Illuminate\Database\Seeder;

use App\Model\User;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'name' => 'Admin', 'email' => 'admin@bit.com', 'password' => '$2y$10$e66JS/tCX5rMFIy9K.IxBu.Q9KCLo207lIPj/MIQFbXdDlJWCJpSW', 'role_id' => 1, 'remember_token' => '', 'image' => ''],
            ['id' => 2, 'name' => 'T1', 'email' => 'T1@yopmail.com', 'password' => '$2y$10$2ecnJGzc4D42frGhH3OXteQEhTxqodfCnf5ko7CerYQSB9dTopOv.', 'role_id' => 2, 'remember_token' => null, 'image' => ''],
            ['id' => 3, 'name' => 'T2', 'email' => 'd1@yopmail.com', 'password' => '$2y$10$.kLamNqb7CyxMDlLH.hj8.i/xeCAhhCTuhcYhLAQLnDjNvNEOioNW', 'role_id' => 2, 'remember_token' => null, 'image' => ''],
            ['id' => 4, 'name' => 'T3', 'email' => 't2@yopmail.com', 'password' => '$2y$10$NZ1uKo.TsFlHJXj7zq637Obpotm5.lgAVx3tlaumK3A3isbc//XJK', 'role_id' => 2, 'remember_token' => null, 'image' => ''],
        ];

        foreach ($items as $item) {
            User::create($item);
        }
    }
}
