<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Task
 *
 * @package App
 * @property string $project
 * @property string $taskname
 * @property text $description
 * @property enum $status
*/
class Task extends Model
{
    use SoftDeletes;

    protected $fillable = ['taskname', 'description', 'status', 'project_id'];
    protected $hidden = [];
    
    

    public static $enum_status = ["Create" => "Create", "In Process" => "In Process", "QA Test" => "QA Test", "Done" => "Done"];

    
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id')->withTrashed();
    }

    // mutators
    public function setProjectIdAttribute($input)
    {
        $this->attributes['project_id'] = $input ? $input : null;
    }
    
    public function setTasknameAttribute($value)
    {
        $this->attributes['taskname'] = strtolower($value);
    }

    // accessor 
    public function getTasknameAttribute($value)
    {
        return ucwords($this->attributes['taskname']);
    }


    // Scope
    /** 
     * Get create task
     *
     */
    public function scopeCreate($query)
    {
        return $query->where('status', 'Create');
    }

    /** 
     * Get In Process task
     *
     */
    public function scopeInProcess($query)
    {
        return $query->where('status', 'In Process');
    }

     /** 
     * Get QA Test task
     *
     */
    public function scopeQATest($query)
    {
        return $query->where('status', 'QA Test');
    }

     /** 
     * Get QA Test task
     *
     */
    public function scopeDone($query)
    {
        return $query->where('status', 'Done');
    }
    
}
