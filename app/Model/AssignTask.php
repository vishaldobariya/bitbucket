<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AssignTask
 *
 * @package App
 * @property string $project
 * @property string $task
 * @property string $developer
*/
class AssignTask extends Model
{
    use SoftDeletes;

    protected $fillable = ['project_id', 'task_id', 'user_id'];
    protected $hidden = [];
    protected $appends = ["username","projectname","taskname"];
    


    public function setProjectIdAttribute($input)
    {
        $this->attributes['project_id'] = $input ? $input : null;
    }

    public function setTaskIdAttribute($input)
    {
        $this->attributes['task_id'] = $input ? $input : null;
    }

    public function setUserIdAttribute($input)
    {
        $this->attributes['user_id'] = $input ? $input : null;
    }
    
    public function getProjectnameAttribute()
    {
        return Project::findOrFail($this->attributes['project_id'])->projectname;
    }

     public function getUsernameAttribute()
    {
        return User::findOrFail($this->attributes['user_id'])->name;
    }

    public function getTasknameAttribute()
    {
        return Task::findOrFail($this->attributes['task_id'])->taskname;
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id')->withTrashed();
    }
    
    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id')->withTrashed();
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    
}
