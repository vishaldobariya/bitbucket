<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Project
 *
 * @package App
 * @property string $projectname
*/
class Project extends Model
{
    use SoftDeletes;

    protected $fillable = ['projectname'];
    protected $hidden = [];
    
    // mutators
    public function setProjectnameAttribute($value)
    {
    	$this->attributes['projectname'] = strtolower(trim($value));
    }

    // accessor 
    public function getProjectnameAttribute($value)
    {
    	return ucwords($this->attributes['projectname']);
    }

    
}
