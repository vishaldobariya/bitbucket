<?php

namespace App\Listeners;

use App\Events\AssignTaskSendEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssignTaskSendEmail  $event
     * @return void
     */
    public function handle(AssignTaskSendEmail $event)
    {
        // send email logic here
    }
}
