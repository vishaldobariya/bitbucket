<?php

namespace app\Helpers;

use Carbon\Carbon;

use App\Model\Project;
use App\Model\Task;
/**
 * 
 */
class Helper 
{
	
	function __construct()
	{
		# code...
	}

	public static function handleGlobalFormat($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }

    public  function getCountProject()
    {
    	return Project::count();
    }

    public  function getCountTask()
    {
    	return Task::count();
    }


}


?>