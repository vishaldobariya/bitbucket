<?php

namespace App\Services;

use Facades\App\Repository\ {
    RoleRepository
};

class RoleService extends RoleRepository
{
   
   public function _construct()
   {
       # code...
   }

   public function create($data)
   {
   	return RoleRepository::create($data);
   }

   public function getData($data)
   {
   	return RoleRepository::getData($data);
   }

   public function updateData($data, $id)
   {
   	return RoleRepository::updateData($data, $id);
   }

   public function findShow($id)
   {
   	return RoleRepository::findShow($id);
   }


}
