<?php

namespace App\Services;

use Facades\App\Repository\ {
    ProjectRepository
};

class ProjectService extends ProjectRepository
{
   
   public function _construct()
   {
       # code...
   }

   public function create($data)
   {
   	return ProjectRepository::create($data);
   }

   public function getData($data)
   {
   	return ProjectRepository::getData($data);
   }

   public function updateData($data, $id)
   {
   	return ProjectRepository::updateData($data, $id);
   }

   public function findShow($id)
   {
   	return ProjectRepository::findShow($id);
   }

   public function whereIn($conditionData)
   {
      return ProjectRepository::whereIn($conditionData);
   }

}
