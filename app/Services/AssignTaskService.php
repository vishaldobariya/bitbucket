<?php

namespace App\Services;

use Facades\App\Repository\ {
    AssignTaskRepository
};

class AssignTaskService extends AssignTaskRepository
{
   
   public function _construct()
   {
       # code...
   }

   public function create($data)
   {
   	return AssignTaskRepository::create($data);
   }

   public function getData($data)
   {
   	return AssignTaskRepository::getData($data);
   }

   public function updateData($data, $id)
   {
   	return AssignTaskRepository::updateData($data, $id);
   }

   public function findShow($id)
   {
   	return AssignTaskRepository::findShow($id);
   }

   public function whereGet($condition)
   {
      return AssignTaskRepository::whereGet($condition);
   }

   public function getLastDoneTask()
   {
      return AssignTaskRepository::getLastDoneTask();
   }

}
