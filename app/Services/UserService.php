<?php

namespace App\Services;

use Facades\App\Repository\ {
    UserRepository
};

class UserService extends UserRepository
{
   
   public function _construct()
   {
       # code...
   }

   public function create($data)
   {
   	return UserRepository::create($data);
   }

   public function getData($data)
   {
   	return UserRepository::getData($data);
   }

   public function updateData($data, $id)
   {
   	return UserRepository::updateData($data, $id);
   }

   public function findShow($id)
   {
   	return UserRepository::findShow($id);
   }

   public function getUsers($id)
   {
      return UserRepository::getUsers($id);
   }

    public function whereGet($condition)
   {
      return UserRepository::whereGet($condition);
   }

   

}
