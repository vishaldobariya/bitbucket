<?php

namespace App\Services;

use Facades\App\Repository\ {
    TaskRepository
};

class TaskService extends TaskRepository
{
   
   public function _construct()
   {
       # code...
   }

   public function create($data)
   {
   	return TaskRepository::create($data);
   }

   public function getData($data)
   {
   	return TaskRepository::getData($data);
   }

   public function updateData($data, $id)
   {
   	return TaskRepository::updateData($data, $id);
   }

   public function findShow($id)
   {
   	return TaskRepository::findShow($id);
   }

   public function enumStatus()
   {
      return TaskRepository::enumStatus();
   }

   public function whereGet($condition)
   {
      return TaskRepository::whereGet($condition);
   }
}
