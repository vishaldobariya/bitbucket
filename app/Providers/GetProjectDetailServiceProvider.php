<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class GetProjectDetailServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Custom Facades
        App::bind('projectdetails', function()
        {
            return new \App\Helpers\Helper;
        });

        App::bind('taskdetails', function()
        {
            return new \App\Helpers\Helper;
        });

        // Custom Service Provider
        App::bind('App\Services\Utillservice', function($data)
        {
            return new Utillservice();
        });

    }
}
