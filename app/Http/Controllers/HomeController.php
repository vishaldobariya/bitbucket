<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;

use App\Facades\ProjectDetails;
use App\Facades\TaskDetails;
use App\Services\Utillservice;

use Facades\App\Services\ {
    TaskService, ProjectService, AssignTaskService, UserService
};

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // Custom service provider
        $utill = new Utillservice();

        // Usigng create custom facades
        $data['projectcount'] = ProjectDetails::getCountProject();
        $data['taskcount'] = TaskDetails::getCountTask();
        $data['totaladmin'] = UserService::getUsers('1')->count();
        $data['totaltl'] = UserService::getUsers('2')->count();
        $data['lasttentask'] = AssignTaskService::getLastDoneTask();
       // dd($data['lasttentask']);
        return view('home', $data);
    }
}
