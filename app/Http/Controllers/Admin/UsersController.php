<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\FileUploadTrait;

use Validator;
use Facades\App\Services\ {
    TaskService, ProjectService, AssignTaskService, UserService, RoleService
};


class UsersController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('user_access')) {
            return abort(401);
        }

        $users = UserService::getData(['status'=>'all']);

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('user_create')) {
            return abort(401);
        }
        
        $roles = RoleService::getData(['status'=>'get'])->pluck('title', 'id')->prepend(trans('translate.please_select'), '');

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('user_create')) {
            return abort(401);
        }
        $rules = [
                'name'    => 'required',
                'email'    => 'required|email|unique:users',
                'password'    => 'required',
                'role_id'    => 'required',
                'image' => 'nullable|mimes:png,jpg,jpeg,gif',
                ];
        $custommsg = [
                'name'    => 'Name',
                'email'    => 'Email',
                'password'    => 'Password',
                'role_id'    => 'Role',
                'image'    => 'Image',
                ];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($custommsg); 
        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }

        $request = $this->saveFiles($request);
        $user = User::create($request->all());



        return redirect()->route('admin.users.index');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('user_edit')) {
            return abort(401);
        }
        
        $roles = Role::get()->pluck('title', 'id')->prepend(trans('translate.please_select'), '');

        $user = User::findOrFail($id);

        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('user_edit')) {
            return abort(401);
        }

       $rules = [
                'name'    => 'required',
                'email'    => 'required|email|unique:users',
                'password'    => 'required',
                'role_id'    => 'required',
                'image' => 'nullable|mimes:png,jpg,jpeg,gif',
                ];
        $custommsg = [
                'name'    => 'Name',
                'email'    => 'Email',
                'password'    => 'Password',
                'role_id'    => 'Role',
                'image'    => 'Image',
                ];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($custommsg); 
        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }


        $request = $this->saveFiles($request);
        $user = UserService::updateData($request, $id);

        if(!empty($task))
        {
            session()->flash('message','User updated successfully');
        }
        else
        {
            session()->flash('error','User not update');
        }
        
        return redirect()->route('admin.users.index');
    }


    /**
     * Display User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('user_view')) {
            return abort(401);
        }
        
        $roles = Role::get()->pluck('title', 'id')->prepend(trans('translate.please_select'), '');$assign_tasks = AssignTask::where('developer_id', $id)->get();

        $user = UserService::findShow($id);

        return view('admin.users.show', compact('user', 'assign_tasks'));
    }


    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('user_delete')) {
            return abort(401);
        }
        $user = UserService::findShow($id);
        $user->delete();

        return redirect()->route('admin.users.index');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('user_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $conditionData = $request->input('ids');
            $entries = UserService::whereIn($conditionData);

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
