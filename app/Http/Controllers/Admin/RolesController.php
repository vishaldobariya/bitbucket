<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use Validator;

use Facades\App\Services\ {
    RoleService
};

class RolesController extends Controller
{
    /**
     * Display a listing of Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('role_access')) {
            return abort(401);
        }


                $roles = RoleService::getData(['status'=>'all']);

        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating new Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('role_create')) {
            return abort(401);
        }
        return view('admin.roles.create');
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param  \App\Http\Requests\StoreRolesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('role_create')) {
            return abort(401);
        }
        $rules = [
                'title'    => 'required',
                ];
        $custommsg = [
                'title'    => 'Title',
                ];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($custommsg); 
        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }

        $role = RoleService::create($request->all());
        if(!empty($role))
        {
            session()->flash('message','Role added successfully');
        }
        else
        {
            session()->flash('error','Role not added');
        }



        return redirect()->route('admin.roles.index');
    }


    /**
     * Show the form for editing Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('role_edit')) {
            return abort(401);
        }
        $role = RoleService::findShow($id);

        return view('admin.roles.edit', compact('role'));
    }

    /**
     * Update Role in storage.
     *
     * @param  \App\Http\Requests\UpdateRolesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('role_edit')) {
            return abort(401);
        }
        $rules = [
                'title'    => 'required',
                ];
        $custommsg = [
                'title'    => 'Title',
                ];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($custommsg); 
        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }

        $role =  RoleService::updateData($request, $id);
        if(!empty($role))
        {
            session()->flash('message','Role update successfully');
        }
        else
        {
            session()->flash('error','Role not updated');
        }


        return redirect()->route('admin.roles.index');
    }


    /**
     * Display Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('role_view')) {
            return abort(401);
        }
        $users = UserService::whereGet(['role_id'=>$id]);
        $role = RoleService::findShow($id);

        return view('admin.roles.show', compact('role', 'users'));
    }


    /**
     * Remove Role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('role_delete')) {
            return abort(401);
        }
        $role = RoleService::findShow($id);
        $role->delete();

        return redirect()->route('admin.roles.index');
    }

    /**
     * Delete all selected Role at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('role_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $conditionData = $request->input('ids');
            $entries = RoleService::whereIn($conditionData);

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
