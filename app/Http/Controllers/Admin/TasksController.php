<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use Validator;

use Facades\App\Services\ {
    TaskService, ProjectService, AssignTaskService, UserService
};

class TasksController extends Controller
{
    /**
     * Display a listing of Task.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('task_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('task_delete')) {
                return abort(401);
            }
            $tasks = TaskService::getData(['status'=>'trash']);
        } else {
            $tasks = TaskService::getData(['status'=>'all']);
        }

        return view('admin.tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating new Task.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('task_create')) {
            return abort(401);
        }
        $projects = ProjectService::getData(['status'=>'get'])->pluck('projectname', 'id')->prepend(trans('translate.please_select'), '');
        $enum_status = TaskService::enumStatus();
            
        return view('admin.tasks.create', compact('enum_status', 'projects'));
    }

    /**
     * Store a newly created Task in storage.
     *
     * @param  Http\Requests\StoreTasksRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('task_create')) {
            return abort(401);
        }
        $rules = [
                'project_id'    => 'required',
                'taskname'    => 'required',
                'status'    => 'required'
                ];
        $custommsg = [
                'project_id'    => 'Project Name',
                'taskname'    => 'Task Name',
                'status'    => 'Status'
                ];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($custommsg); 
        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }

        $task = TaskService::create($request->all()); 
        if(!empty($task))
        {
            session()->flash('message','Task added successfully');
        }
        else
        {
            session()->flash('error','Task not added');
        }

        return redirect()->route('admin.tasks.index');
    }


    /**
     * Show the form for editing Task.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('task_edit')) {
            return abort(401);
        }
        
        $projects = ProjectService::getData(['status'=>'get'])->pluck('projectname', 'id')->prepend(trans('translate.please_select'), '');
        $enum_status = TaskService::enumStatus();
            
        $task = TaskService::findShow($id);

        return view('admin.tasks.edit', compact('task', 'enum_status', 'projects'));
    }

    /**
     * Update Task in storage.
     *
     * @param  Http\Requests\UpdateTasksRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('task_edit')) {
            return abort(401);
        }
        $rules = [
                'project_id'    => 'required',
                'taskname'    => 'required',
                'status'    => 'required'
                ];
        $custommsg = [
                'project_id'    => 'Project Name',
                'taskname'    => 'Task Name',
                'status'    => 'Status'
                ];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($custommsg); 
        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }

        $task = TaskService::updateData($request, $id);

        if(!empty($task))
        {
            session()->flash('message','Task updated successfully');
        }
        else
        {
            session()->flash('error','Task not update');
        }
        
        return redirect()->route('admin.tasks.index');
    }


    /**
     * Display Task.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('task_view')) {
            return abort(401);
        }
        
        $projects = Project::get()->pluck('projectname', 'id')->prepend(trans('translate.please_select'), '');
        $assign_tasks = AssignTask::where('task_id', $id)->get();

        $task = TaskService::findShow($id);

        return view('admin.tasks.show', compact('task', 'assign_tasks'));
    }


    /**
     * Remove Task from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('task_delete')) {
            return abort(401);
        }
        $task = TaskService::findShow($id);
        $task->delete();

        return redirect()->route('admin.tasks.index');
    }

    /**
     * Delete all selected Task at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('task_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Task::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Task from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('task_delete')) {
            return abort(401);
        }
        $task = TaskService::getData(['status'=>'trashed'])->findOrFail($id);
        $task->restore();

        return redirect()->route('admin.tasks.index');
    }

    /**
     * Permanently delete Task from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('task_delete')) {
            return abort(401);
        }
        $task = TaskService::getData(['status'=>'trashed'])->findOrFail($id);
        $task->forceDelete();

        return redirect()->route('admin.tasks.index');
    }
}
