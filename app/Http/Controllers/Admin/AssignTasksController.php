<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Events\AssignTaskSendEmail;
use Validator;
use Facades\App\Services\ {
    TaskService, ProjectService, AssignTaskService, UserService
};

class AssignTasksController extends Controller
{
    /**
     * Display a listing of AssignTask.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('assign_task_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('assign_task_delete')) {
                return abort(401);
            }
            $assign_tasks = AssignTaskService::getData(['status'=>'trash']);
        } else {
            $assign_tasks = AssignTaskService::getData(['status'=>'all']);
        }

        return view('admin.assign_tasks.index', compact('assign_tasks'));
    }

    /**
     * Show the form for creating new AssignTask.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('assign_task_create')) {
            return abort(401);
        }
        
        $projects = ProjectService::getData(['status'=>'get'])->pluck('projectname', 'id')->prepend(trans('translate.please_select'), '');
        $tasks = collect(TaskService::getData(['status'=>'get']));
        $users = UserService::whereGet(['role_id'=>'2'])->pluck('name', 'id')->prepend(trans('translate.please_select'), '');
        // dd($tasks);
        return view('admin.assign_tasks.create', compact('projects', 'tasks', 'users'));
    }

    /**
     * Store a newly created AssignTask in storage.
     *
     * @param  \App\Http\Requests\StoreAssignTasksRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('assign_task_create')) {
            return abort(401);
        }
        $rules = [
                'project_id'    => 'required',
                'task_id'    => 'required',
                'user_id'    => 'required',
                ];
        $custommsg = [
                'project_id'    => 'Project Name',
                'task_id'    => 'Task Name',
                'user_id'    => 'User Name',
                ];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($custommsg); 
        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }
        $assign_task = AssignTaskService::create($request->all()); 
        if(!empty($assign_task))
        {
             // Send email to Assign task user.
            event(new AssignTaskSendEmail($assign_task));
            session()->flash('message','Assign task added successfully');
        }
        else
        {
            session()->flash('message','Assign task fail');
        }


        return redirect()->route('admin.assign_tasks.index');
    }


    /**
     * Show the form for editing AssignTask.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('assign_task_edit')) {
            return abort(401);
        }
        
        $projects = ProjectService::getData(['status'=>'get'])->pluck('projectname', 'id')->prepend(trans('translate.please_select'), '');
        $tasks = collect(TaskService::getData(['status'=>'get']));
        $users = UserService::whereGet(['role_id'=>'2'])->pluck('name', 'id')->prepend(trans('translate.please_select'), '');

        $assign_task = AssignTaskService::findShow($id);
       // dd($assign_task);

        return view('admin.assign_tasks.edit', compact('assign_task', 'projects', 'tasks', 'users'));
    }

    /**
     * Update AssignTask in storage.
     *
     * @param  \App\Http\Requests\UpdateAssignTasksRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('assign_task_edit')) {
            return abort(401);
        }
         $rules = [
                'project_id'    => 'required',
                'task_id'    => 'required',
                'user_id'    => 'required',
                ];
        $custommsg = [
                'project_id'    => 'Project Name',
                'task_id'    => 'Task Name',
                'user_id'    => 'User Name',
                ];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($custommsg); 
        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }

        $assign_task =  AssignTaskService::updateData($request, $id);

        if(!empty($assign_task))
        {
             // Send email to Assign task user.
            event(new AssignTaskSendEmail($assign_task));
            session()->flash('message','Assign task updated successfully');
        }
        else
        {
            session()->flash('message','Not Assign task');
        }

        return redirect()->route('admin.assign_tasks.index');
    }


    /**
     * Display AssignTask.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('assign_task_view')) {
            return abort(401);
        }
        $assign_task = AssignTaskService::findShow($id);

        return view('admin.assign_tasks.show', compact('assign_task'));
    }


    /**
     * Remove AssignTask from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('assign_task_delete')) {
            return abort(401);
        }
        $assign_task = AssignTaskService::findShow($id);
        $assign_task->delete();

        return redirect()->route('admin.assign_tasks.index');
    }

    /**
     * Delete all selected AssignTask at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('assign_task_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = AssignTaskService::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore AssignTask from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('assign_task_delete')) {
            return abort(401);
        }
        $assign_task = AssignTaskService::getData(['status'=>'trashed'])->findOrFail($id);
        $assign_task->restore();

        return redirect()->route('admin.assign_tasks.index');
    }

    /**
     * Permanently delete AssignTask from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('assign_task_delete')) {
            return abort(401);
        }
        $assign_task = AssignTaskService::getData(['status'=>'trashed'])->findOrFail($id);
        $assign_task->forceDelete();

        return redirect()->route('admin.assign_tasks.index');
    }
}
