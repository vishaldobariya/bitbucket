<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Events\AssignTaskSendEmail;
use Validator;


use Facades\App\Services\ {
    TaskService, ProjectService, AssignTaskService, UserService
};

class ProjectsController extends Controller
{


    /**
     * Display a listing of Project.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if (! Gate::allows('project_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('project_delete')) {
                return abort(401);
            }
            $projects = ProjectService::getData(['status'=>'trash']);
        } else {
            $projects = ProjectService::getData(['status'=>'all']);
        }

        return view('admin.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating new Project.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('project_create')) {
            return abort(401);
        }
        return view('admin.projects.create');
    }

    /**
     * Store a newly created Project in storage.
     *
     * @param  \App\Http\Requests\StoreProjectsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('project_create')) {
            return abort(401);
        }

        $rules = ['projectname'    => 'required|unique:projects'];
        $custommsg = ['projectname'    => 'Project Name'];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($custommsg); 
        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }


        $project = ProjectService::create($request->all());

        if(!empty($project))
        {
            session()->flash('message','Project added successfully');
        }
        else
        {
            session()->flash('error','Project not added');
        }

        return redirect()->route('admin.projects.index',compact('errors'));
    }


    /**
     * Show the form for editing Project.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        if (! Gate::allows('project_edit')) {
            return abort(401);
        }
        $project = ProjectService::findShow($id);
        return view('admin.projects.edit', compact('project'));
    }

    /**
     * Update Project in storage.
     *
     * @param  \App\Http\Requests\UpdateProjectsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('project_edit')) {
            return abort(401);
        }
        $rules = ['projectname'    => 'required|unique:projects'];
        $custommsg = ['projectname'    => 'Project Name'];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($custommsg); 
        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        }

        $project = ProjectService::updateData($request, $id);
        if(!empty($project))
        {
            session()->flash('message','Project update successfully');
        }
        else
        {
            session()->flash('error','Project not added');
        }
        return redirect()->route('admin.projects.index');
    }


    /**
     * Display Project.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('project_view')) {
            return abort(401);
        }
        $tasks = TaskService::whereGet(['project_id'=>$id]); 
        $assign_tasks = AssignTaskService::whereGet(['project_id'=>$id]); 
        $project = ProjectService::findShow($id);
       
        return view('admin.projects.show', compact('project', 'tasks', 'assign_tasks'));
    }


    /**
     * Remove Project from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('project_delete')) {
            return abort(401);
        }
        $project = ProjectService::findShow($id);
        $project->delete();

        return redirect()->route('admin.projects.index');
    }

    /**
     * Delete all selected Project at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('project_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $conditionData =$request->input('ids');
            $entries = ProjectService::whereIn($conditionData);

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Project from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('project_delete')) {
            return abort(401);
        }
        $project = ProjectService::getData(['status'=>'trashed'])->findOrFail($id);
        $project->restore();
         session()->flash('message','Project restore successfully');

        return redirect()->route('admin.projects.index');
    }

    /**
     * Permanently delete Project from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('project_delete')) {
            return abort(401);
        }
        $project = ProjectService::getData(['status'=>'trashed'])->findOrFail($id);
        $project->forceDelete();
        session()->flash('message','Project permanently delete successfully');
        return redirect()->route('admin.projects.index');
    }
}
