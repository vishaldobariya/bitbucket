<?php

namespace App\Http\Middleware;

use Closure;
use View;

class Loadjscss
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $datatableLoad = false;
       // echo $request->route()->uri;
        $alloweddt = ["admin/roles", "admin/users", "admin/projects",  "admin/tasks", "admin/assign_tasks", "admin/roles/{role}",  "admin/users/{user}",  "admin/projects/{project}",  "admin/tasks/{task}"];
        if(in_array($request->route()->uri, $alloweddt)){
            $datatableLoad = true;
        }
        View::share("datatableLoad",$datatableLoad);
        return $next($request);
    }
}
