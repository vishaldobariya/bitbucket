<?php

namespace App\Repository;

use App\Model\AssignTask;

class AssignTaskRepository 
{
   
   public function _construct()
   {
       # code...
   }


   public function create($data)
   {
   		return AssignTask::create($data);
   }

   public function getData($data)
   {
   	if($data['status'] == 'trash')
   	{
   		return AssignTask::onlyTrashed()->get();
   	}
   	elseif ($data['status'] == 'all') {
   		return AssignTask::all();
   	}
    elseif ($data['status'] == 'get') {
      return AssignTask::get();
    }
   }

   public function updateData($data, $id)
   {
   		$AssignTask = AssignTask::findOrFail($id);
       $AssignTask->update($data->all());
       return $AssignTask;
   }

    public function findShow($id)
   {
   	return AssignTask::findOrFail($id);
   }

   public function whereGet($condition)
   {
      return AssignTask::where($condition)->get();
   }
   public function getLastDoneTask()
   {

       $user =  Assigntask::with(['task' => function($query)
      {
          $query->Done()->OrderBy('updated_at');

      }])->with(['user' => function($query)
      {
        $query->whereRoleId(2);
      }])->limit(10)->get()->toArray();
      return $user;
   }
    
}
