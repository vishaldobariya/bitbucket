<?php

namespace App\Repository;

use App\Model\Project;

class ProjectRepository 
{
   
   public function _construct()
   {
       # code...
   }


   public function create($data)
   {
   		return Project::create($data);
   }

   public function getData($data)
   {
   	if($data['status'] == 'trash')
   	{
   		return Project::onlyTrashed()->get();
   	}
   	elseif ($data['status'] == 'all') {
   		return Project::all();
   	}
   	elseif ($data['status'] == 'get') {
   		return Project::get();
   	}
      elseif ($data['status'] == 'trashed') {
         return Project::onlyTrashed();
      }
   }

   public function updateData($data, $id)
   {
   	 $project = Project::findOrFail($id);
        $project->update($data->all());
        return $project;
   }

    public function findShow($id)
   {
   	return Project::findOrFail($id);
   }

   public function whereIn($conditionData)
   {
      return  Project::whereIn('id', $conditionData)->get();
   }

}
