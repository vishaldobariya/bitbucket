<?php

namespace App\Repository;

use App\Model\Task;

class TaskRepository 
{
   
   public function _construct()
   {
       # code...
   }


   public function create($data)
   {
   		return Task::create($data);
   }

   public function getData($data)
   {
   	if($data['status'] == 'trash')
   	{
   		return Task::onlyTrashed()->get();
   	}
   	elseif ($data['status'] == 'all') {
   		return Task::all();
   	}
    elseif ($data['status'] == 'get') {
      return Task::get();
    }
   }

   public function updateData($data, $id)
   {
   		$task = Task::findOrFail($id);
       $task->update($data->all());
       return $task;
   }

    public function findShow($id)
   {
   	return Task::findOrFail($id);
   }

    public function enumStatus()
   {
      return Task::$enum_status;
   }

   public function whereGet($condition)
   {
      return Task::where($condition)->get();
   }
}
