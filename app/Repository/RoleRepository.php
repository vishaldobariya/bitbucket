<?php

namespace App\Repository;

use App\Model\Role;

class RoleRepository 
{
   
   public function _construct()
   {
       # code...
   }


   public function create($data)
   {
   		return Role::create($data);
   }

   public function getData($data)
   {
   	if($data['status'] == 'trash')
   	{
   		return Role::onlyTrashed()->get();
   	}
   	elseif ($data['status'] == 'all') {
   		return Role::all();
   	}
   	elseif ($data['status'] == 'get') {
   		return Role::get();
   	}
      elseif ($data['status'] == 'trashed') {
         return Role::onlyTrashed();
      }
   }

   public function updateData($data, $id)
   {
   	 $Role = Role::findOrFail($id);
        $Role->update($data->all());
        return $Role;
   }

    public function findShow($id)
   {
   	return Role::findOrFail($id);
   }

   public function whereIn($conditionData)
   {
      return  Role::whereIn('id', $conditionData)->get();
   }

   
}
