<?php

namespace App\Repository;

use App\Model\User;

class UserRepository 
{
   
   public function _construct()
   {
       # code...
   }


   public function create($data)
   {
   		return User::create($data);
   }

   public function getData($data)
   {
   	if($data['status'] == 'trash')
   	{
   		return User::onlyTrashed()->get();
   	}
   	elseif ($data['status'] == 'all') {
   		return User::all();
   	}
    elseif ($data['status'] == 'get') {
      return User::get();
    }
   }

   public function updateData($data, $id)
   {
   		$User = User::findOrFail($id);
       $User->update($data->all());
       return $User;
   }

    public function findShow($id)
   {
   	return User::findOrFail($id);
   }

   public function getUsers($id)
   {
      return User::whereRoleId($id)->get();
   }
    
    public function whereGet($condition)
   {
      return User::where($condition)->get();
   }

   
}
