<?php

namespace Vishal\Smallbit;

use Illuminate\Support\ServiceProvider;

class BitbucketServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/packageroutes.php');
        $this->loadViewsFrom(__DIR__.'/views', 'smallbit');
        $this->publishes([__DIR__.'/assets' => public_path('vishalassets')], 'smallbit');
    }
}
